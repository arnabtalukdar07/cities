import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http:HttpClient) { }
  getData(){
    return this.http.get(' http://api.minebrat.com/api/v1/states');
  }
  getcities(stateId){
    return this.http.get(' http://api.minebrat.com/api/v1/states/cities/'+stateId);
  }
}

