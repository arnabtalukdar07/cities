import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CoursesComponent } from './courser.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListComponent } from './Components/list/list.component';
import { CitiesListComponent } from './Components/cities-list/cities-list.component';
import { ResultComponent } from './Components/result/result.component';
import { AppRoutingModule } from './app-routing.module'

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    ListComponent,
    CitiesListComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,HttpClientModule,ReactiveFormsModule, AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
