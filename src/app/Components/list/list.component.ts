import { ApiService } from './../../Services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { IncomingMessage } from 'http';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  selectStateCity=this.fb.group({
    state:['',Validators.required]
  })

  show1=false;
  stateId;
  cities;

  states_list;
  constructor(public api:ApiService,
    public fb:FormBuilder,public router:Router) {
  this.api.getData().subscribe((data)=>{
    console.log(data);
    this.states_list=data;
  })
   }

  ngOnInit(): void {
  }
  postMessage(messageFromChild) {
    console.log(messageFromChild);
    //call service/api to post message
  }
  onStateChange(){
    this.show1=this.selectStateCity.valid;
    this.stateId=this.selectStateCity.get('state').value;
    for(let state of this.states_list){
      if(this.stateId==state.stateId){
        console.log(state.stateName);
        localStorage.setItem("mState",state.stateName);
      }
    }
    // localStorage.setItem("mState",this.stateId);
    this.api.getcities(this.stateId).subscribe((data)=>{
      console.log(data);
      this.cities=data;
    })
  }

  show(){
    this.show1=true;
    console.log(this.stateId);
    this.router.navigateByUrl('/result');
  }

}
