import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  @Input() city;

@Input() state;
  constructor() { }

  ngOnChanges(changes:SimpleChanges){
    console.log(changes);
    if(changes.city){
    this.city=changes.city.currentValue;
    }
    else{
    this.state=changes.state.currentValue;
    }
    
  }
  ngOnInit(): void {
  }

}
