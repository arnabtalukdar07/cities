import { Router } from '@angular/router';
import { Component, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import * as EventEmitter from 'events';
import { ApiService } from 'src/app/Services/api.service';

@Component({
  selector: 'app-cities-list',
  templateUrl: './cities-list.component.html',
  styleUrls: ['./cities-list.component.scss']
})
export class CitiesListComponent implements OnInit {
@Input() cities;
// @Input() show2;
@Input() stateId;
@Output() countChanged: EventEmitter =   new EventEmitter();
  selectStateCity=this.fb.group({
    city:['',Validators.required]
  })

  show2=false;
  show1=false;
  state=this.selectStateCity.valid;
  data;
  stateN;
  msubmit="false";
  cityId;
  sbutton=true;
  constructor(public api:ApiService,
    public fb:FormBuilder,public router:Router) {
  }

  ngOnChanges(changes:SimpleChanges){
    console.log(changes);
    if(changes.cities){
      this.cities=changes.cities.currentValue;
    }
    else if(changes.stateId){
      
    this.router.navigateByUrl('');
      this.stateId=changes.stateId.currentValue;
      this.show1=false;
      this.show2=false;
      this.sbutton=true;
      
    }
    
   
    
  }

  ngOnInit(): void {
  }
  oncityChange(){
    this.router.navigateByUrl('');
    this.show2=true;
    
    this.cityId=this.selectStateCity.get('city').value;
    this.state=this.selectStateCity.valid;
      this.show1=false;
      if(this.state && this.show2){
        this.sbutton=false;
      }
    
    console.log(this.cityId)
    for(let state of this.cities){
      if(this.cityId==state.cityId){
        this.cityId=state.cityName;
      }
    }

  }

  show(){
    this.show1=true;
    this.api.getData().subscribe((res)=>{
      this.data=res;
      for(let state of this.data){
        if(this.stateId==state.stateId){
          this.stateN=state.stateName;
        }
      }
      console.log(this.stateN);
      this.router.navigateByUrl('result')
      
    })
  }
}