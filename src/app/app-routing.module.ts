import { ListComponent } from './Components/list/list.component';
import { ResultComponent } from './Components/result/result.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component:ListComponent
  },
  {
    path: 'result',
    component:ResultComponent
    
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
